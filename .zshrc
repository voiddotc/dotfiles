# voiddotc's zsh init config
 
setopt autocd
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[red]%}@%{$fg[yellow]%}%m%{$fg[red]%}] %{$fg[yellow]%}%~ %{$fg[yellow]%}>%{$fg[red]%}>%{$reset_color%}%b "

HISTSIZE=123456789
SAVEHIST=123456789
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/shell_history"
stty stop undef

# autocomplete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# show hidden files
_comp_options+=(globdots)

# vim keybinds
bindkey -v
export KEYTIMEOUT=1

# vim in menu select
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'j' vi-forward-char

# beam cursor
echo -ne '\e[6 q'

# env vars
export BEMENU_OPTS="-b -p '' --tb #285577 --hb #285577 --tf #eeeeee --hf #eeeeee --nf #bbbbbb"


# plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
